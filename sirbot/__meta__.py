DATA = {
    # Ordered by commit log entries
    "author": ('mikefromit <mike.arbelaez@gmail.com>, '
               'Matt Rasband <matt.rasband@gmail.com>, '
               'Ovv <ovv@outlook.com>, '
               'Nick Humrich <nick.humrich@gmail.com>, '
               'Julioocz <julioocz@gmail.com>, '
               'Shawn McElroy <shawn@skift.io>, '
               'Sean Johnson <Sean.Johnson@iag.com.au>'),
    "author_email": 'pythondev.slack@gmail.com',
    "copyright": 'Copyright 2016 Python Developers Community',
    "description": 'The good Sir Bot a lot',
    "license": 'MIT',
    "name": 'sirbot',
    "url": 'https://gitlab.com/mikefromit/sirbot',
    # Versions should comply with PEP440. For a discussion on
    # single-sourcing the version across setup.py and the project code,
    # see http://packaging.python.org/en/latest/tutorial.html#version
    "version": '0.0.1',
    'docker_name': 'sirbot_api',
    'docker_tag': 'latest'
}
